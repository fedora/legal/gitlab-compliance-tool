# gitlab-compliance-tool

Simple tool to check project visibility and license listing per GitLab's
[Open Source Program](https://about.gitlab.com/solutions/open-source/join/).


# Building

## Source

```bash
$ poetry build -f sdist
Building gitlab-compliance-tool (0.1.1)
  - Building sdist
  - Built gitlab_compliance_tool-0.1.1.tar.gz
$
```
## Container
Build the source tarball and then ...

```bash
$ podman build -f Containerfile -t gct:0.1.1
[..]
COMMIT gct:0.1.1
--> d84edb27207a
Successfully tagged localhost/gct:0.1.1
d84edb27207a87834bdd5169d2bc68ea6979c9e10ae5557f1c471e9e6adc9aae
$ 
```

# Usage

## Installed from source

Command line input
```bash
$ /usr/local/bin/gct --group-id=12345 --private-token=secret
[..]
```

Environment Variables
```bash
$ cat gct.env
GCT_GROUP_ID=12345
GCT_PRIVATE_TOKEN=secret
$ . gct.env
$ /usr/local/bin/gct
[..]
```

## Container

ommand line input
```bash
$ podman run gct:0.1.1 --group-id=12345 --private-token=secret
[..]
```

Environment Variables
```bash
$ cat gct.env
GCT_GROUP_ID=12345
GCT_PRIVATE_TOKEN=secret
$ podman run --env-file gct.env gct:0.1.1
[..]
```
