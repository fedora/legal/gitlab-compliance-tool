# This file is part of gitlab-compliance-tool.
#
# Copyright (C) 2023 Steve Milner
#
# SPDX-License-Identifier: GPL-3.0-or-later

import os
import subprocess

import pytest


def _run(cli_args: list) -> subprocess.Popen:
    """
    Runs the CLI from source inside of poetry.
    """
    args = ["poetry", "run", "python3", "gct/__main__.py"]
    args = args + cli_args
    cmd = subprocess.Popen(
        args,
        env={"PYTHONPATH": os.getcwd()},
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    # Wait for up to 20 seconds for the command to finish
    # before killing the process
    cmd.wait(20.0)
    return cmd


def test_help():
    """
    Ensure --help works.
    """
    cmd = _run(["--help"])
    assert cmd.returncode == 0
    assert len(cmd.stderr.readlines()) == 0
    assert len(cmd.stdout.readlines()) > 1


def test_bare_cli():
    """
    Ensure running with no options notifies the user of missing input.
    """
    cmd = _run([])
    assert cmd.returncode == 1
    assert len(cmd.stderr.readlines()) == 1
    assert len(cmd.stdout.readlines()) == 0
