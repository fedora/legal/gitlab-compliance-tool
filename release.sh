#!/bin/bash
# This file is part of gitlab-compliance-tool.
#
# Copyright (C) 2023 Steve Milner
#
# SPDX-License-Identifier: GPL-3.0-or-later

set -euf -o pipefail

if [[ $# -ne 1 ]]; then
  echo "You must provide a new version number for the release"
  exit 1
fi

NEW_VERSION="$1"

if [ -z "${NEW_VERSION}" ]; then
    echo "The new version number may not be null or an empty string"
    exit 1
fi

# If the dist directory exists, move it out of the way
# instead of deleting. We want to be nice just in case!
if [ -d dist ]; then
    TMPDIR=$(mktemp -d)
    mv dist $TMPDIR/
    echo "+ Moved old dist/ to ${TMPDIR}"
fi

# Check code conventions
BLACK_CMD="$(command -v black)"
if [ $? -eq 0  ]; then
    PYTHONPATH=`pwd` ${BLACK_CMD} --check gct/
    if [ $? -ne 0 ]; then
        echo "- Code convention check failed. See output. Exiting ..."
        exit 1
    fi
else
   echo "!!!WARNING!!! black was not found. Skipping code convention verification"
   read -p "Do you want to continue with this release? (y/N): " NO_BLACK
   if [[ "${NO_BLACK,,}" != "y" && "${NO_BLACK,,}" != "yes" ]]; then
           exit 1
   fi
fi

# Check unittests
PYTEST_CMD="$(command -v pytest)"
if [ $? -eq 0  ]; then
    PYTHONPATH=`pwd` ${PYTEST_CMD}
    if [ $? -ne 0 ]; then
        echo "- Unittesting failed. See output. Exiting ..."
        exit 1
    fi
else
   echo "!!!WARNING!!! pytest was not found. Skipping code unittest verification"
   read -p "Do you want to continue with this release? (y/N): " NO_PYTEST
   if [[ "${NO_PYTEST,,}" != "y" && "${NO_PYTEST,,}" != "yes" ]]; then
           exit 1
   fi
fi

# Update versions
CURRENT_VERSION=$(poetry version -s)
echo "+ Current version is ${CURRENT_VERSION}. Updating to ${NEW_VERSION}"
sed -i "s|version = \"${CURRENT_VERSION}\"|version = \"${NEW_VERSION}\"|" pyproject.toml
sed -i "s|__version__ = \"${CURRENT_VERSION}\"|__version__ = \"${NEW_VERSION}\"|" gct/__init__.py
sed -i "s|release = '${CURRENT_VERSION}'|release = '${NEW_VERSION}'|" docs/conf.py

# Show the diff
git diff
read -p "Should we commit this diff in preparation for release? (y/N): " COMMIT
if [[ "${COMMIT,,}" != "y" && "${COMMIT,,}" != "yes" ]]; then
    echo "+ Exiting without commit..."
    exit 0
fi

git add pyproject.toml gct/__init__.py docs/conf.py

COMMIT_FILE=$(mktemp)
echo -e "Updating for release ${NEW_VERSION}\n" > ${COMMIT_FILE}
git log --format="* %h %as %s" --since=${CURRENT_VERSION} >> ${COMMIT_FILE}
git commit -F ${COMMIT_FILE}
git tag "${NEW_VERSION}"

echo "+ Creating source tarball"
poetry build -f sdist
tar tzf dist/gitlab_compliance_tool-"${NEW_VERSION}".tar.gz
sha256sum dist/gitlab_compliance_tool-"${NEW_VERSION}".tar.gz
echo "+ Done"
